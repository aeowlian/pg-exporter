
export class ConvertToCSVService {

  convertToCSV(array: any[], isPajak?: boolean, multiplier?: number) {
    let csvString = 'sep=;\n';

    let columnFormat;

    if(isPajak) {
      columnFormat = [
        {
          columnName: 'No. Faktur',
          prop: 'no_bukti',
          type: 'string'
        },
        {
          columnName: 'Tanggal',
          prop: 'tgl',
          type: 'string'
        },
        {
          columnName: 'Buyer',
          prop: 'buyer',
          type: 'string'
        },
        {
          columnName: 'Kiloan',
          prop: 'qty',
          type: 'number'
        },
        {
          columnName: 'Lembar',
          prop: 'lembar',
          type: 'number'
        },
        {
          columnName: 'DPP',
          prop: 'total',
          type: 'number'
        },
        {
          columnName: 'PPN',
          prop: 'ppn',
          type: 'number'
        },
        {
          columnName: 'Total',
          prop: 'grandTotal',
          type: 'number'
        }
      ]
    }else{
      columnFormat = [
        {
          columnName: 'ID Data',
          prop: 'id_data',
          type: 'string'
        },
        {
          columnName: 'No. Faktur',
          prop: 'no_bukti',
          type: 'string'
        },
        {
          columnName: 'Tanggal',
          prop: 'tgl',
          type: 'string'
        },
        {
          columnName: 'Buyer',
          prop: 'buyer',
          type: 'string'
        },
        {
          columnName: 'Nama',
          prop: 'nama',
          type: 'string'
        },
        {
          columnName: 'Warna',
          prop: 'warna',
          type: 'string'
        },
        {
          columnName: 'Harga',
          prop: 'harga',
          type: 'number'
        },
        {
          columnName: 'Lembar',
          prop: 'lembar',
          type: 'number'
        },
        {
          columnName: 'Kiloan',
          prop: 'qty',
          type: 'number'
        },
        {
          columnName: 'Total',
          prop: 'total',
          type: 'number'
        }
      ]
    }

    columnFormat.forEach(oneFormat => {
      csvString += oneFormat.columnName + ';';
    })

    // Remove trailing delimiter
    csvString = csvString.slice(0, csvString.length-1);
    csvString += "\n";

    let fmt = new Intl.NumberFormat('id-ID', 
    {
      maximumFractionDigits: 2,
      useGrouping: false
    });

    array.forEach(element => {
      columnFormat.forEach(format => {
        if(format.prop == 'tgl') {
          let date = this.getDeepMember(element, format.prop) as Date;
          csvString += date.toISOString() + ';';
        }else if(format.type == 'number') {
          csvString += fmt.format(this.getDeepMember(element, format.prop)) + ';';
        }else{
          csvString += this.getDeepMember(element, format.prop) + ';';
        }
      })

      if(multiplier != null) {
        let kilogram = this.getDeepMember(element, 'qty');
        csvString += kilogram * multiplier;
      }else{
        csvString = csvString.slice(0, csvString.length-1);
      }

      csvString += "\n";
    });

    return csvString;
  }

  private getDeepMember = (from: any, memberString: string) => {
    let attributeStack = memberString.split('.');
    let obj: any = from;
    attributeStack.forEach(attribute => {
      obj = obj[attribute];
    });
    return obj;
  }
}

export class CSVFormat {
  columnName: string;
  prop: string;
  type: string;
}