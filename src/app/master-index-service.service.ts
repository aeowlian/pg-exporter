// import { readFileSync } from "fs";

export class MasterIndexService {

  // private async getReader() {
  //     let modules = await import('mdb-reader');
  //     let buffer = readFileSync("./STOCK.mdb");
  //     return new modules.default(buffer);
  // }

  // private async getTable(tableName: string) {
  //     return (await this.getReader()).getTable(tableName);
  // }
  isPajak = false;
  seenMasterBarang = [];
  masterJual = [];
  jualDetails = [];
  buyer = [];
  numberOfBuyer = 0;
  numberOfItems = 0;
  numberOfJuals = 0;
  numberOfJualDetails = 0;
  penjualan: any[] = [];

  async indexMaster(buffer) {
    let modules = await import('mdb-reader');
    // let buffer;
    // try{
    //   buffer = readFileSync("./STOCK.mdb");
    // }catch (e) {
    // }
    let reader = new modules.default(buffer);
    let masterBarang = reader.getTable('masterbrg').getData();
    
    this.seenMasterBarang = [];
    
    this.numberOfItems = 0;


    masterBarang.forEach(element => {
        this.seenMasterBarang[(element.kode as string)] = element;
        this.numberOfItems++;
    })
  }

  getMasterBarang() {
      return this.seenMasterBarang;
  }

  async indexJualDetail(buffer, date) {
    let modules = await import('mdb-reader');
    // let buffer;
    // try{
    //   buffer = readFileSync("./STOCK.mdb");
    // }catch (e) {
    // }
    let reader = new modules.default(buffer);
    let tabelJualMaster;
    let tabelJualDetail;
    let tryGetTable;
    let jualDetailFromTable;

    try{
      tryGetTable = reader.getTable('jualpajak_mas');
    }catch (e) {
    }
    if(tryGetTable) {
      this.isPajak = true;
      tabelJualDetail = 'jualpajak_det';
      tabelJualMaster = 'jualpajak_mas';
      jualDetailFromTable = reader.getTable(tabelJualDetail).getData();
    }else{
      tabelJualDetail = 'jlbaku_det';
      tabelJualMaster = 'jlbaku_mas';
      jualDetailFromTable = await this.joinTable(reader.getTable(tabelJualDetail), buffer);
    }

    let jualMasterFromTable = reader.getTable(tabelJualMaster).getData();
    let buyer = reader.getTable('buyer').getData();

    buyer.forEach(element => {
      this.buyer[(element.KODE as string)] = element;
      this.numberOfBuyer++;
    });

    let masterjuals = [];

    jualMasterFromTable.forEach(element => {
      if(this.masterJual[(element.no_bukti as string)] != null) {
        throw new Error("Ada Penjualan dengan nomor bukti duplikat.");
      }
      let today = date;
      today.setHours(0, 0, 0, 0);
      let elementTgl = new Date(element.tgl as string);
      elementTgl.setHours(0, 0, 0, 0);
      if(elementTgl.getTime() == today.getTime()) {
        this.masterJual[(element.no_bukti as string)] = element;
        this.numberOfJuals++;
        let buyer = this.buyer[(element.buyer as string)];
        element.buyerDetail = buyer;
        masterjuals.push(element);
      }
      // if(isTodayOnly) {
      // }else{
      //   this.masterJual[(element.no_bukti as string)] = element;
      //   this.numberOfJuals++;
      //   let buyer = this.buyer[(element.buyer as string)];
      //   element.buyerDetail = buyer;
      // }
    });


    let result = [];

    jualDetailFromTable.forEach(element => {
      // this.jualDetails[(element.kode as string)] = element;
      this.numberOfJualDetails++;
      let masterJual = this.masterJual[element['no_bukti'] as string];
      if(masterJual != null) {
        if(masterJual.entries == null) {
          masterJual.entries = [];
        }
        element.tgl = masterJual.tgl;
        element.buyer = masterJual.buyerDetail.NAMA;
        element.harga = element.harga;
        let ppn = 0;
        let total = 0;
        if(element.lembar > 0) {
          total = element.harga * element.lembar;
        }else{
          total = element.harga * element.qty;
        }
        element.total = total;
        if(this.isPajak) {
          element.ppn = element.total * masterJual.ppn / 100;
          element.grandTotal = total + (element.total * masterJual.ppn / 100);
        }
        masterJual.entries.push(element);
        result.push(element);
      }
    })

    if(this.isPajak) {
      let pajakResult = [];
      masterjuals.forEach(element => {
        console.log(element);
        let total = 0;
        let ppn = 0;
        let grandTotal = 0;
        let lembar = 0;
        let kiloan = 0;
        (element as any).entries.forEach(entry => {
          total += entry.total;
          ppn += entry.ppn;
          grandTotal += entry.grandTotal;
          lembar += entry.lembar;
          kiloan += entry.qty;
        });
        element.no_bukti = (element.no_bukti as string).slice(2, element.no_bukti.length)
        element.buyer = element.buyerDetail.NAMA;
        element.total = total;
        element.ppn = ppn;
        element.grandTotal = grandTotal;
        element.lembar = lembar;
        element.qty = kiloan;
        pajakResult.push(element);
      });
      console.log(pajakResult);
      result = pajakResult;
    }

    result.sort((a,b) => {
      return (a.no_bukti as string) < (b.no_bukti as string) ? -1 : 1;
    });


    this.penjualan = result;
  }

  private kodeNamaPipe(element) {
      return {kode: element.kode, nama: element.nama, kodealias: element.kodealias}
  }

  private async joinTable(table, buffer) {
      let reindexed = false;
      let seenMasterBarang = this.getMasterBarang();
      
      let data = table.getData();
      let dataLength = table.rowCount;
      let result = [];
      let map = new Map();
      let nullCounter = 0;
  
      if(dataLength > 0) {
        if(data[0].kode != null) {
          for (let i = 0; i < dataLength; i++) {
            const element = data[i];
            if(element == null) break;

            if(element.saldo1 == 0) continue;
            
            if(seenMasterBarang[(element.kode as string)] == null && !reindexed) {
              await this.indexMaster(buffer);
              seenMasterBarang = this.getMasterBarang();
              reindexed = true;
            }
            if(seenMasterBarang[(element.kode as string)] != null) {
              element.nama = seenMasterBarang[(element.kode as string)].nama;
              element.kodealias = seenMasterBarang[(element.kode as string)].kodealias;
            }
            result.push(element);
            if(element == null) nullCounter++;
            map.set(element.kode, element);
          }
        }
      }
      return result;
    }

  // async getAllStockTables() {
  //   let tables = [];
    
  //   tables['stockafalTable'] = await this.getTable('stockafal');
  //   tables['stockbahanbaku_kTable'] = await this.getTable('stockbahanbaku_k');
  //   tables['stocklembaranTable'] = await this.getTable('stocklembaran');
  //   tables['stockpackgagangTable'] = await this.getTable('stockpackgagang');
  //   tables['stockpackpoundTable'] = await this.getTable('stockpackpound');
  //   tables['stockppTable'] = await this.getTable('stockpp');
  //   tables['stockrollgagangTable'] = await this.getTable('stockrollgagang');
  //   tables['stockrollpolosTable'] = await this.getTable('stockrollpolos');
  //   tables['stockbahanbakuTable'] = await this.getTable('stockbahanbaku');
  //   tables['stockrollsablonTable'] = await this.getTable('stockrollsablon');

  //   return tables;
  // }

  // async indexAllStock() {

  //   let tables = await this.getAllStockTables();
  //   // await this.indexStock('getStockAfal', (await this.joinTable(tables['stockafalTable'])).map(this.kodeNamaPipe));
  //   await this.indexStock('getStockBahanBakuKecil', (await this.joinTable(tables['stockbahanbaku_kTable'])).map(this.kodeNamaPipe));        
  //   await this.indexStock('getStockLembaran', (await this.joinTable(tables['stocklembaranTable'])).map(this.kodeNamaPipe));        
  //   await this.indexStock('getStockPackingGagang', (await this.joinTable(tables['stockpackgagangTable'])).map(this.kodeNamaPipe));        
  //   await this.indexStock('getStockPackingPound', (await this.joinTable(tables['stockpackpoundTable'])).map(this.kodeNamaPipe));        
  //   await this.indexStock('getStockPP', (await this.joinTable(tables['stockppTable'])).map(this.kodeNamaPipe));        
  //   await this.indexStock('getStockRollGagang', (await this.joinTable(tables['stockrollgagangTable'])).map(this.kodeNamaPipe));        
  //   await this.indexStock('getStockRollPolos', (await this.joinTable(tables['stockrollpolosTable'])).map(this.kodeNamaPipe));        
  //   await this.indexStock('getStockBahanBaku', (await this.joinTable(tables['stockbahanbakuTable'])).map(this.kodeNamaPipe));        
  //   await this.indexStock('getStockRollSablon', (await this.joinTable(tables['stockrollsablonTable'])).map(this.kodeNamaPipe));
  // }

  // async indexStock(indexName: string, objects: any[]) {
  //     let index = searchClient.initIndex(indexName);
  //     await index.clearObjects();
  //     await index.saveObjects(objects, {autoGenerateObjectIDIfNotExist: true});
  // }
}
