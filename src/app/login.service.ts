export class LoginService {
  async login(buffer, username: string, password: string) {
    let modules = await import('mdb-reader');

    let reader = new modules.default(buffer);
    let users = reader.getTable('tbluser').getData();

    for (let i = 0; i < users.length; i++) {
      const user = users[i];
      if(user.USER == username && user.PASS == password) {
        return true;
      }
    }

    return false;
  }

  async getUsers(buffer) {
    let modules = await import('mdb-reader');

    let reader = new modules.default(buffer);
    let users = reader.getTable('tbluser').getData();
    let result = [];

    users.forEach(user => {
      result.push(user.USER);
    })
    
    return result;
  }
}