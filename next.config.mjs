/** @type {import('next').NextConfig} */
const nextConfig = {output: 'export', distDir: 'dist', basePath: '/pg-exporter'};

export default nextConfig;
